package org.coolimc.system.systemapi;

import java.lang.management.ManagementFactory;

import com.sun.management.OperatingSystemMXBean;

public class HardwareInforation
{
	private HardwareInforation()
	{
		//Do Nothing instead of Being Private
	}
	
	public static int getProcessorCores()
	{
		OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		return (operatingSystemMXBean.getAvailableProcessors());
	}
	
	public static long getTotalMemorySize()
	{
		OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		return (operatingSystemMXBean.getTotalPhysicalMemorySize());
	}
	
	public static long getFreeMemorySize()
	{
		OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		return (operatingSystemMXBean.getFreePhysicalMemorySize());
	}
}
