package org.coolimc.system.systemapi;

public class OperatingSystem
{
	public static final int OS_UNKNOWN = 0;
	public static final int OS_UNIX_LINUX = 1;
	public static final int OS_WINDOWS = 2;
	public static final int OS_SOLARIS = 3;
	public static final int OS_FREEBSD = 4;
	public static final int OS_MAC = 5;
	
	public static final int OS_UNKNOWNBIT = 0;
	public static final int OS_32BIT = 32;
	public static final int OS_64BIT = 64;
	
	private static final String OS = System.getProperty("os.name").toLowerCase();
	private static final String Architecture = System.getProperty("os.arch").toLowerCase();
	
	private OperatingSystem()
	{
		//DO Nothing instead of Being Private
	}
	
	public static String getOSName()
	{
		return System.getProperty("os.name");
	}
	
	public static String getOSVersion()
	{
		return System.getProperty("os.version");
	}
	
	public static String getOSArchitecture()
	{
		return System.getProperty("os.arch");
	}
	
	public static boolean is64Bit()
	{
		return (Architecture.indexOf("64") >= 0);
	}
	
	public static boolean is32Bit()
	{
		return (Architecture.indexOf("32") >= 0);
	}
	
	public static boolean isWindows()
	{
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isMac()
	{
		return (OS.indexOf("mac") >= 0);
	}

	public static boolean isUnix_Linux()
	{
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}

	public static boolean isSolaris()
	{
		return (OS.indexOf("sunos") >= 0);
	}
	
	public static boolean isFreeBSD()
	{
		return (OS.indexOf("freebsd") >= 0);
	}
}
