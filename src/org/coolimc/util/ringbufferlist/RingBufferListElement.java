package org.coolimc.util.ringbufferlist;

class RingBufferListElement<E extends Comparable<E>>
{
	private final E elementPointer;
	
	private RingBufferListElement<E> right;
	private RingBufferListElement<E> left;
	
	public RingBufferListElement(E e)
	{
		this.elementPointer = e;
	}
	
	public void setRight(RingBufferListElement<E> nRight)
	{
		this.right = nRight;
	}
	
	public RingBufferListElement<E> getRight()
	{
		return this.right;
	}
	
	public void setLeft(RingBufferListElement<E> nLeft)
	{
		this.left = nLeft;
	}
	
	public RingBufferListElement<E> getLeft()
	{
		return this.left;
	}
	
	public E getElementPointer()
	{
		return this.elementPointer;
	}
}
