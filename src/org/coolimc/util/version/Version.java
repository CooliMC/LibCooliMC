package org.coolimc.util.version;

import java.util.List;
import org.coolimc.util.string.StringExtension;

public class Version
{
	public final static String DEFAULT_PART_STRING = new String(".");
	
	private final String version_id;
	private final String part_String;
	
	public Version(int versionTag, int... versionTags)
	{
		this(DEFAULT_PART_STRING, versionTag, versionTags);
	}

	public Version(String partString, int versionTag, int... versionTags)
	{
		String versionBuild = new String("");
		
		versionBuild += versionTag;
		for(int tempVersionTag : versionTags)
		{
			versionBuild += partString;
			versionBuild += tempVersionTag;
		}
		
		this.version_id = versionBuild;
		this.part_String = partString;
	}
	
	public String getVersion()
	{
		return this.version_id;
	}
	
	public boolean isGivenVersionHigher(String gVersion)
	{
		return Version.isGivenVersionHigher(this.version_id, gVersion, this.part_String);
	}
	
	public boolean isGivenVersionLower(String gVersion)
	{
		return Version.isGivenVersionLower(this.version_id, gVersion, this.part_String);
	}
	
	public boolean equalsGivenVersion(String gVersion)
	{
		return (!this.isGivenVersionHigher(gVersion) && !this.isGivenVersionLower(gVersion));
	}
	
	public String getVersionStringWithCharsMaxLength(int maxLength, String partChar, String character)
	{
		if(maxLength <= this.version_id.length() || character == null || character.length() == 0) return this.version_id;
		
		String partCharacter = ((partChar == null || partChar.length() == 0) ? "" : partChar);
		
		int characterLength = (maxLength - (version_id.length() + partChar.length()));
		int charcterMulti = (((characterLength % character.length()) == 0) ? (characterLength / character.length()) : ((characterLength / character.length()) + 1));
		
		String toRet = (version_id + partCharacter);
		for(int i = 0; i < charcterMulti; i++) toRet += character;
		
		if(toRet.length() > maxLength) toRet = toRet.substring(0, maxLength);
		
		return toRet;
	}
	
	public static boolean isGivenVersionHigher(String oVersion, String nVersion, String splitter)
	{
		int oVersionLength = Version.getGivenVersionLength(oVersion, splitter);
		int nVersionLength = Version.getGivenVersionLength(nVersion, splitter);
		
		int maxLength = ((oVersionLength > nVersionLength) ? oVersionLength : nVersionLength);
		
		for(int i = 1; i <= maxLength; i++)
		{
			int indexVersionO = Version.getGivenVersionPart(oVersion, splitter, i);
			int indexVersionN = Version.getGivenVersionPart(nVersion, splitter, i);
			
			if(indexVersionO < indexVersionN) return true;
			else if(indexVersionO > indexVersionN) return false;
		}
		
		return false;
	}
	
	public static boolean isGivenVersionLower(String oVersion, String nVersion, String splitter)
	{
		int oVersionLength = Version.getGivenVersionLength(oVersion, splitter);
		int nVersionLength = Version.getGivenVersionLength(nVersion, splitter);
		
		int maxLength = ((oVersionLength > nVersionLength) ? oVersionLength : nVersionLength);
		
		for(int i = 1; i <= maxLength; i++)
		{
			int indexVersionO = Version.getGivenVersionPart(oVersion, splitter, i);
			int indexVersionN = Version.getGivenVersionPart(nVersion, splitter, i);
			
			if(indexVersionO > indexVersionN) return true;
			else if(indexVersionO < indexVersionN) return false;
		}
		
		return false;
	}
	
	private static int getGivenVersionPart(String gVersion, String splitter, int position)
	{
		if(gVersion == null || gVersion.length() == 0) return 0;
		if(position < 1) return 0;
		
		List<String> splitted = StringExtension.splitStringCharSafeList(gVersion, splitter, true);
		if(splitted.size() < position) return 0;
		
		String pos = splitted.get(position - 1);
		if(pos == null || pos.length() == 0) return 0;
		
		try { return Integer.parseInt(pos); } catch(Exception e) { return 0; }
	}

	private static int getGivenVersionLength(String gVersion, String splitter)
	{
		if(gVersion == null || gVersion.length() == 0) return 0;
		
		return (StringExtension.splitStringCharSafeList(gVersion, splitter, true).size());
	}
}