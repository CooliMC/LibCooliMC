package org.coolimc.system.systemapi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ProcessAndProgramInformation
{
	private ProcessAndProgramInformation()
	{
		//DO Nothing instead of Being Private
	}
	
	public static List<List<String[]>> getProcessList()
	{
		List<List<String[]>> toRet = null;
		
		if(OperatingSystem.isWindows())
		{
			toRet = new ArrayList<List<String[]>>();
			
			try {
			    // Execute command
			    String command = "tasklist /v /fo CSV";
			    Process child = Runtime.getRuntime().exec(command);

			    // Get output stream to read from it
			    BufferedReader reader = new BufferedReader(new InputStreamReader(child.getInputStream()));
			    String tempStr = null;
			    
			    String[] columnNames;
			    int columns = 0;
			    
			    if((tempStr = reader.readLine()) != null)
			    {
			    	if(tempStr.length() > 0)
			    	{
				    	String[] splitted = tempStr.split("\",\"");
				    	columns = splitted.length;
			    		
			    		//Korrekt First and Last
			    		if((splitted.length > 0) && (splitted[0].length() > 0))
			    			splitted[0] = (splitted[0].substring(1, splitted[0].length()));
			    		
			    		if((splitted.length > 0) && (splitted[(splitted.length - 1)].length() > 0))
			    			splitted[splitted.length -1 ] = (splitted[splitted.length - 1].substring(0, (splitted[splitted.length - 1].length() - 1)));
			    		
			    		columnNames = new String[columns];
			    		for(int i = 0; i < splitted.length; i++)
			    			columnNames[i] = splitted[i];
				    	
			    		if(columns > 0)
			    		{
			    			String[] columnValues = new String[columns];
			    			List<String[]> toAdd = new ArrayList<String[]>();
			    			
				    		while((tempStr = reader.readLine()) != null)
						    {
						    	if(tempStr.length() > 0)
						    	{
							    	columnValues = tempStr.split("\",\"");
							    	
							    	//Korrekt First and Last
						    		if((columnValues.length > 0) && (columnValues[0].length() > 0))
						    			columnValues[0] = (columnValues[0].substring(1, columnValues[0].length()));
						    		
						    		if((columnValues.length > 0) && (columnValues[(columnValues.length - 1)].length() > 0))
						    			columnValues[columnValues.length -1 ] = (columnValues[columnValues.length - 1].substring(0, (columnValues[columnValues.length - 1].length() - 1)));
							    	
							    	if(columnValues.length == columns)
							    	{
							    		toAdd = new ArrayList<String[]>();
							    		
							    		for(int i = 0; i < columnValues.length; i++)
							    		{
							    			String[] toAddArr = new String[2];
							    			
							    			toAddArr[0] = columnNames[i];
							    			toAddArr[1] = columnValues[i];
							    			
							    			toAdd.add(toAddArr);
							    		}
							    		
							    		if(toAdd.size() == columns) toRet.add(toAdd);
							    	}
						    	}
						    }
			    		}
			    	}
			    }
			    
			   
			} catch (Exception e) {
				toRet = null;
			}
		} else {
			//NOT SUPPORTED YET	
		}
		
		return toRet;
	}
}
