package org.coolimc.system.systemapi;

public class SystemUser
{
	private SystemUser()
	{
		//DO Nothing instead of Being Private
	}
	
	public static String getCountry()
	{
		return System.getProperty("user.country");
	}
	
	public static String getHomePath()
	{
		return System.getProperty("user.home");
	}
	
	public static String getTimezone()
	{
		return System.getProperty("user.timezone");
	}
	
	public static String getUsername()
	{
		return System.getProperty("user.name");
	}
	
	public static String getUserLanguage()
	{
		return System.getProperty("user.language");
	}
}
