package org.coolimc.util.string;

import java.util.ArrayList;
import java.util.List;

public class StringExtension
{
	private StringExtension()
	{
		//Keep private
	}
	
	public static String[] splitStringCharSafeArray(String givenString, String splitter, boolean caseSensitive)
	{
		return StringExtension.splitStringCharSafeList(givenString, splitter, caseSensitive).toArray(new String[0]);
	}
	
	public static List<String> splitStringCharSafeList(String givenString, String splitter, boolean caseSensitive)
	{
		List<String> toRet = new ArrayList<String>();
		
		if(givenString == null || splitter == null || splitter.length() == 0)
			return toRet;
		
		String tempString = new String("");
		for(int index = 0; index < givenString.length(); index++)
		{
			if((givenString.charAt(index) == splitter.charAt(0)) && ((givenString.length() - index) >= splitter.length()))
			{
				String subOfGiven = givenString.substring(index, index + (splitter.length()));
				
				if(subOfGiven.equals(splitter) || (!caseSensitive && subOfGiven.toLowerCase().equals(splitter.toLowerCase())))
				{
					toRet.add(tempString);
					tempString = new String("");
					index += (splitter.length() - 1);
				} else {
					tempString += givenString.charAt(index);
				}
			} else {
				tempString += givenString.charAt(index);
			}
		}
		
		toRet.add(tempString);
		
		return toRet;
	}
}
