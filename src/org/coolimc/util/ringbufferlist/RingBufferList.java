package org.coolimc.util.ringbufferlist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class RingBufferList<E extends Comparable<E>> implements List<E>
{
	private RingBufferListPosition<E> currentPosition;
	
	public RingBufferList()
	{
		this.currentPosition = new RingBufferListPosition<E>();
	}
	
	public int size()
	{
		return this.currentPosition.getSize();
	}

	public boolean isEmpty()
	{
		return (this.size() == 0);
	}

	public boolean contains(Object o)
	{
		return (this.indexOf(o) >= 0);
	}
	
	public boolean contains(E e)
	{
		return (this.indexOf(e) >= 0);
	}

	public Iterator<E> iterator()
	{
		return this.toArrayList().iterator();
	}

	public Object[] toArray()
	{
		return this.toArrayList().toArray();
	}

	public <T> T[] toArray(T[] a)
	{
		return this.toArrayList().toArray(a);
	}

	public boolean add(E e)
	{
		return this.currentPosition.add(e);
	}

	public boolean remove(Object o)
	{
		return this.currentPosition.remove(o);
	}
	
	public boolean remove(E e)
	{
		return this.currentPosition.remove(e);
	}

	public boolean containsAll(Collection<?> c)
	{
		return this.toArrayList().containsAll(c);
	}

	public boolean addAll(Collection<? extends E> c)
	{
		return this.currentPosition.addAll(c);
	}

	public boolean addAll(int index, Collection<? extends E> c)
	{
		return this.currentPosition.addAll(index, c);
	}

	public boolean removeAll(Collection<?> c)
	{
		return this.currentPosition.removeAll(c);
	}

	public boolean retainAll(Collection<?> c)
	{
		return this.currentPosition.retainAll(c);
	}

	public void clear()
	{
		this.currentPosition.clear();
	}

	public E get(int index)
	{
		return this.currentPosition.get(index);
	}

	public E set(int index, E element)
	{
		return this.currentPosition.set(index, element);
	}

	public void add(int index, E element)
	{
		this.currentPosition.add(index, element);
	}

	public E remove(int index)
	{
		return this.currentPosition.remove(index);
	}

	public int indexOf(Object o)
	{
		return this.currentPosition.indexOf(o);
	}
	
	public int indexOf(E e)
	{
		return this.currentPosition.indexOf(e);
	}

	public int lastIndexOf(Object o)
	{
		return this.currentPosition.lastIndexOf(o);
	}
	
	public int lastIndexOf(E e)
	{
		return this.currentPosition.lastIndexOf(e);
	}

	public ListIterator<E> listIterator()
	{
		return this.toArrayList().listIterator();
	}

	public ListIterator<E> listIterator(int index)
	{
		return this.toArrayList().listIterator(index);
	}

	public List<E> subList(int fromIndex, int toIndex)
	{
		return this.toArrayList().subList(fromIndex, toIndex);
	}
	
	public ArrayList<E> toArrayList()
	{
		return this.currentPosition.toArrayList();
	}
	
	public int setPosition(E e)
	{
		return this.currentPosition.setPosition(e);
	}
	
	public E setPosition(int direction)
	{
		return this.currentPosition.setPosition(direction);
	}
}
