package org.coolimc.network.update;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.coolimc.system.systemapi.OperatingSystem;
import org.coolimc.system.systemapi.SystemInformation;

public class Updater
{
	private final String updateURL;
	private final String nameOfJar;
	private final String nameOfUpdateJar;
	
	private final List<UpdateResponseHandler> updateResponseHandlerList;

	public Updater(String updateURL, String nameOfOldJar, String nameOfUpdateJar)
	{
		if (updateURL == null || updateURL.length() == 0)
			throw new IllegalArgumentException("Empty updateURL.");

		if (nameOfOldJar == null || nameOfOldJar.length() == 0)
			throw new IllegalArgumentException("Empty nameOfOldJar.");

		if (nameOfUpdateJar == null || nameOfUpdateJar.length() == 0)
			throw new IllegalArgumentException("Empty nameOfUpdateJar.");

		this.updateURL = updateURL;
		this.nameOfJar = nameOfOldJar;
		this.nameOfUpdateJar = nameOfUpdateJar;
		
		this.updateResponseHandlerList = new ArrayList<UpdateResponseHandler>();
	}

	public boolean updateIfAvailable()
	{
		File latestVersion = downloadLatestUpdate(this.updateURL);
		if(latestVersion == null || !latestVersion.exists() || !latestVersion.isFile())
		{
			this.fireUpdateResponseHandlerMessageIncoming("An error occured while downloding the latest version of the Application.");
			return false;
		}
		
		this.fireUpdateResponseHandlerMessageIncoming("The Application will be closed soon and the Updater will try to install the latest Version.");
		boolean successfullUpdate = this.installUpdate();
		
		if(!successfullUpdate)
		{
			this.fireUpdateResponseHandlerMessageIncoming("An error occured while trying to install the latest Version.");
			return false;
		}
		
		return true;
	}
	
	private boolean installUpdate()
	{
		if (OperatingSystem.isWindows())
			if (!this.installUpdateWindows())
				return false;
			else if (OperatingSystem.isUnix_Linux())
				if (!this.installUpdateUnix())
					return false;
				else
					return false;

		// Start a timer for killing the Programm
		new Thread() {
			public void run() {
				try {
					Thread.sleep(2500);
				} catch (Exception e) {
				}
				System.exit(0);
			}
		}.start();

		// And inform the Program
		this.fireUpdateResponseHandlerUpdateInstallationIncoming();

		return true;
	}

	private boolean installUpdateWindows()
	{
		try {
			String batchCommands = new String("");
			String continueString = new String(" && ");

			batchCommands += ("ping 127.0.0.1 -n 6 > nul" + continueString);
			batchCommands += ("(if exist \"" + nameOfJar + "\" ( del -f \"" + nameOfJar + "\" ))" + continueString);
			batchCommands += ("ren \"" + nameOfUpdateJar + "\" \"" + nameOfJar + "\"" + continueString);
			batchCommands += ("start \"Update\" \"" + nameOfJar + "\" -b");
			;

			Runtime.getRuntime().exec("cmd /c \"" + batchCommands + "\"");
		} catch (Exception e) {
			this.fireUpdateResponseHandlerMessageIncoming(
					"An error occured while trying to start the Updater on Windows.");
			return false;
		}

		return true;
	}

	private boolean installUpdateUnix()
	{
		try {
			String shellCommands = new String("");
			String continueString = new String(" && ");

			shellCommands += ("sleep 5" + continueString);
			shellCommands += ("rm -f \"" + this.nameOfJar + "\"" + continueString);
			shellCommands += ("mv \"" + this.nameOfUpdateJar + "\" \"" + this.nameOfJar + "\"" + continueString);
			shellCommands += ("nohup java -jar \"" + this.nameOfJar + "\" >>/dev/null 2>>/dev/null &");

			Runtime.getRuntime().exec(shellCommands);
		} catch (Exception e) {
			this.fireUpdateResponseHandlerMessageIncoming(
					"An error occured while trying to start the Updater on Unix.");
			return false;
		}

		return true;
	}

	private File downloadLatestUpdate(String downloadURL)
	{
		this.fireUpdateResponseHandlerMessageIncoming(
				"The Application is trying to download the File from the given File-URL.");

		URL dURL = null;
		File destinationFile = null;

		// Check for Download URL
		try {
			dURL = new URL(downloadURL);
		} catch (Exception e) {
			dURL = null;
			this.fireUpdateResponseHandlerMessageIncoming(
					"The given downloadURL doesn't match a normal URl and will be wasted.");
		}
		;

		// Check for File
		try {
			destinationFile = new File(SystemInformation.getPathOfCurrentJar(), nameOfUpdateJar);
		} catch (Exception e) {
			destinationFile = null;
			this.fireUpdateResponseHandlerMessageIncoming(
					"An error occured while trying to create the UpdateFile on the HardDrive.");
		}
		;

		if ((dURL == null) || (destinationFile == null))
			return null;

		try {
			FileUtils.copyURLToFile(dURL, destinationFile, 10000, 10000);
		} catch (Exception e) {
			this.fireUpdateResponseHandlerMessageIncoming(
					"An error occured while trying to download the give URL-File, check you internet connection or try again later.");
			return null;
		}

		return destinationFile;
	}

	// Handler
	private void fireUpdateResponseHandlerMessageIncoming(String text)
	{
		for (UpdateResponseHandler updateResponseHandler : this.updateResponseHandlerList)
			updateResponseHandler.onNewMessageIncoming(text);
	}

	private void fireUpdateResponseHandlerUpdateInstallationIncoming()
	{
		for (UpdateResponseHandler updateResponseHandler : this.updateResponseHandlerList)
			updateResponseHandler.onUpdateInstallationIncoming();
	}

	public List<UpdateResponseHandler> getAllUpdateResponseHandlers()
	{
		return this.updateResponseHandlerList;
	}

	public boolean addUpdateResponseHandler(UpdateResponseHandler handler)
	{
		return this.updateResponseHandlerList.add(handler);
	}

	public boolean removeUpdateResponseHandler(UpdateResponseHandler handler)
	{
		return this.updateResponseHandlerList.remove(handler);
	}

	public void removeAllUpdateResponseHandlers()
	{
		this.updateResponseHandlerList.clear();
	}
}