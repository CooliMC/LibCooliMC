package org.coolimc.network.update;

public interface UpdateResponseHandler
{
	void onNewMessageIncoming(String msg);
	void onUpdateInstallationIncoming();
}
