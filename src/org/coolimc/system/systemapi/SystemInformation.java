package org.coolimc.system.systemapi;

import java.io.File;
import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

public class SystemInformation
{
	private SystemInformation()
	{
		//Do Nothing instead of Being Private
	}
	
	public static String getOSName()
	{
		return OperatingSystem.getOSName();
	}
	
	public static String getOSVersion()
	{
		return OperatingSystem.getOSVersion();
	}
	
	public static String getOSArchitecture()
	{
		return OperatingSystem.getOSArchitecture();
	}
	
	public static int getOS()
	{
		if(OperatingSystem.isWindows()) return OperatingSystem.OS_WINDOWS;
		if(OperatingSystem.isUnix_Linux()) return OperatingSystem.OS_UNIX_LINUX;
		if(OperatingSystem.isSolaris()) return OperatingSystem.OS_SOLARIS;
		if(OperatingSystem.isFreeBSD()) return OperatingSystem.OS_FREEBSD;
		if(OperatingSystem.isMac()) return OperatingSystem.OS_MAC;
		
		return OperatingSystem.OS_UNKNOWN;
	}
	
	public static int getArchitecture()
	{
		if(OperatingSystem.is32Bit()) return OperatingSystem.OS_32BIT;
		if(OperatingSystem.is64Bit()) return OperatingSystem.OS_64BIT;
		
		return OperatingSystem.OS_UNKNOWNBIT;
	}
	
	public static String getCountry()
	{
		return SystemUser.getCountry();
	}
	
	public static String getLanguage()
	{
		return SystemUser.getUserLanguage();
	}
	
	public static String getHomePath()
	{
		return SystemUser.getHomePath();
	}
	
	public static String getPathOfCurrentJar()
	{
		try {
			String urlPath = URLDecoder.decode(ClassLoader.getSystemClassLoader().getResource(".").getPath(), "UTF-8");
			return (new File(urlPath).getAbsolutePath()); 
		} catch(Exception e) {
			return (new String(""));
		}
	}
	
	public static boolean doesCurrentJarRunsCMD()
	{
		return (System.console() != null);
	}
	
	public static String getTimezone()
	{
		return SystemUser.getTimezone();
	}
	
	public static String getUsername()
	{
		return SystemUser.getUsername();
	}
	
	public static String getJavaVersion()
	{
		return System.getProperty("java.version");
	}
	
	public static String getLocalTime()
	{
		return getLocalTime(":");
	}
	
	public static String getLocalTime(String partString)
	{
		LocalDateTime ldt = LocalDateTime.now();
		
		return (
			((ldt.getHour() < 10) ? ("0" + ldt.getHour()) : ldt.getHour()) + partString +
			((ldt.getMinute() < 10) ? ("0" + ldt.getMinute()) : ldt.getMinute()) + partString +
			((ldt.getSecond() < 10) ? ("0" + ldt.getSecond()) : ldt.getSecond())
		);
	}
	
	public static String getLocalDate()
	{
		return getLocalDate(".");
	}
	
	public static String getLocalDate(String partString)
	{
		LocalDateTime ldt = LocalDateTime.now();
		return (
			((ldt.getDayOfMonth() < 10) ? ("0" + ldt.getDayOfMonth()) : ldt.getDayOfMonth()) + partString +
			((ldt.getMonthValue() < 10) ? ("0" + ldt.getMonthValue()) : ldt.getMonthValue()) + partString +
			ldt.getYear()
		);
	}
	
	public static String getTimezoneTime()
	{
		return getTimezoneTime(":");
	}
	
	public static String getTimezoneTime(String partString)
	{
		ZonedDateTime zdt = ZonedDateTime.now();
		return (
				((zdt.getHour() < 10) ? ("0" + zdt.getHour()) : zdt.getHour()) + partString +
				((zdt.getMinute() < 10) ? ("0" + zdt.getMinute()) : zdt.getMinute()) + partString +
				((zdt.getSecond() < 10) ? ("0" + zdt.getSecond()) : zdt.getSecond())
		);
	}
	
	public static String getTimezoneDate()
	{
		return getTimezoneDate(".");
	}
	
	public static String getTimezoneDate(String partString)
	{
		ZonedDateTime zdt = ZonedDateTime.now();
		return (
				((zdt.getDayOfMonth() < 10) ? ("0" + zdt.getDayOfMonth()) : zdt.getDayOfMonth()) + partString +
				((zdt.getMonthValue() < 10) ? ("0" + zdt.getMonthValue()) : zdt.getMonthValue()) + partString +
				zdt.getYear()
		);
	}
	
	public static int getProcessorCores()
	{
		return HardwareInforation.getProcessorCores();
	}
	
	public static long getTotalMemorySize()
	{
		return HardwareInforation.getTotalMemorySize();
	}
	
	public static long getFreeMemorySize()
	{
		return HardwareInforation.getFreeMemorySize();
	}
	
	public static List<List<String[]>> getProcessList()
	{
		return ProcessAndProgramInformation.getProcessList();
	}
}
