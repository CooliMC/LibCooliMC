# LibCooliMC
<h2>My Private Library</h2>

Many Classes for different special usecases like:
   * Version - Small class for version control of project, compare two versions, get higher or lower version and so on
   * RingBufferList - Generic RingBufferList to get last element much faster and change firstElement without removing and adding latest first Element again
   * Update - Small extension that can be used in runnable Jars for self updating by give URL supported Unix and Windows
   * StringExtension
      * SplitStringCharSafe - Now you can split your Strings without case sensitivity or split by "." without "\\\\."
   * SystemHardwareUserInformation
      * HardwareInformation - Informations about your Hardware like cpu count or memory count
      * OperatingSystem - Get the OperatingSystem, check if Windows, Mac, Unix and so on
      * ProcessAndProgramInformation - Information about all processes and programs running
      * SystemInformation - Many informations about the system like Java-Version or path or the current running jar
      * SystemUser - Many informations about the user like his home path and so on
