package org.coolimc.util.ringbufferlist;

import java.util.ArrayList;
import java.util.Collection;

class RingBufferListPosition<E extends Comparable<E>>
{
	private RingBufferListElement<E> currentPosition;
	
	public RingBufferListPosition()
	{
		this.currentPosition = null;
	}
	
	public RingBufferListPosition(RingBufferListElement<E> curPos)
	{
		this.currentPosition = curPos;
	}

	public int getSize()
	{
		int toRet = 0;
		
		if(this.currentPosition != null)
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			toRet = 1;
			
			for(; tempBuf != this.currentPosition; toRet++)
				tempBuf = tempBuf.getRight();
		}
		
		return toRet;
	}

	public boolean add(E e)
	{
		if(e == null) return false;
		
		RingBufferListElement<E> tempElement = new RingBufferListElement<E>(e);
		
		if(this.currentPosition != null)
		{
			tempElement.setRight(this.currentPosition);
			tempElement.setLeft(this.currentPosition.getLeft());
			
			tempElement.getLeft().setRight(tempElement);
			tempElement.getRight().setLeft(tempElement);
		} else {
			tempElement.setLeft(tempElement);
			tempElement.setRight(tempElement);
			
			this.currentPosition = tempElement;
		}
		
		return true;
	}
	
	public boolean add(int index, E e)
	{
		if(e == null) return false;
		
		int sIndex = this.shortIndex(index);
		if((index != 0) && (sIndex == 0)) return this.add(e);
		
		RingBufferListElement<E> tempElement = new RingBufferListElement<E>(e);
		
		if(this.currentPosition != null)
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			
			if(sIndex >= 0)
			{
				for(int counter = 0; counter != sIndex;)
				{
					tempBuf = tempBuf.getRight();
					counter++;
				}
			} else {
				for(int counter = 0; counter != sIndex;)
				{
					tempBuf = tempBuf.getLeft();
					counter--;
				}
			}
			
			tempElement.setRight(tempBuf);
			tempElement.setLeft(tempBuf.getLeft());
			
			tempElement.getLeft().setRight(tempElement);
			tempElement.getRight().setLeft(tempElement);
			
			if(index == 0) this.currentPosition = tempElement;
		} else {
			tempElement.setLeft(tempElement);
			tempElement.setRight(tempElement);
			
			this.currentPosition = tempElement;
		}
		
		return true;
	}
	
	public int indexOf(E e)
	{
		if((this.currentPosition != null) && (e != null))
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			int counter = 0;
			
			do {
				if(tempBuf.getElementPointer().compareTo(e) == 0)
					return counter;
				
				counter++;
				tempBuf = tempBuf.getRight();
			} while(tempBuf != this.currentPosition);
		}
		
		return -1;
	}
	
	public int indexOf(Object o)
	{
		if((this.currentPosition != null) && (o != null))
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			int counter = 0;
			
			do {
				if(tempBuf.getElementPointer().equals(o))
					return counter;
				
				counter++;
				tempBuf = tempBuf.getRight();
			} while(tempBuf != this.currentPosition);
		}
		
		return -1;
	}
	
	public int lastIndexOf(E e)
	{
		int toRet = -1;
		
		if((this.currentPosition != null) && (e != null))
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			int counter = 0;
			
			do {
				if(tempBuf.getElementPointer().compareTo(e) == 0)
					toRet = counter;
				
				counter++;
				tempBuf = tempBuf.getRight();
			} while(tempBuf != this.currentPosition);
		}
		
		return toRet;
	}
	
	public int lastIndexOf(Object o)
	{
		int toRet = -1;
		
		if((this.currentPosition != null) && (o != null))
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			int counter = 0;
			
			do {
				if(tempBuf.getElementPointer().equals(o))
					toRet = counter;
				
				counter++;
				tempBuf = tempBuf.getRight();
			} while(tempBuf != this.currentPosition);
		}
		
		return toRet;
	}
	
	public boolean remove(E e)
	{
		return this.removeElement(this.getElement(e));
	}
	
	public boolean remove(Object o)
	{
		return this.removeElement(this.getElement(o));
	}
	
	public E remove(int index)
	{
		RingBufferListElement<E> tempElement = this.getElement(index);
		
		return (this.removeElement(tempElement) ? tempElement.getElementPointer() : null);
	}

	public E get(int index)
	{
		RingBufferListElement<E> tempElement = this.getElement(index);
		
		return ((tempElement != null) ? tempElement.getElementPointer() : null);
	}
	
	public void clear()
	{
		if(this.currentPosition != null)
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			
			do {
				tempBuf = tempBuf.getRight();
				
				tempBuf.getLeft().setRight(null);
				tempBuf.setLeft(null);
			} while(tempBuf != this.currentPosition);
			
			this.currentPosition = null;
		}
	}
	
	public ArrayList<E> toArrayList()
	{
		ArrayList<E> toRet = new ArrayList<E>();
		
		if(this.currentPosition != null)
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			
			do {
				toRet.add(tempBuf.getElementPointer());
				tempBuf = tempBuf.getRight();
			} while(tempBuf != this.currentPosition);
		}
		
		return toRet;
	}

	
	public boolean addAll(Collection<? extends E> toAddList)
	{
		boolean toRet = false;
		
		if(toAddList != null)
			for(E listItem : toAddList)
				toRet |= this.add(listItem);
		
		return toRet;
	}

	public boolean addAll(int index, Collection<? extends E> toAddList)
	{
		boolean toRet = false;
		
		if(toAddList != null)
		{
			if(this.currentPosition != null)
			{
				int sIndex = this.shortIndex(index);
				RingBufferListElement<E> tempIndexElement = this.getElement(sIndex);
				RingBufferListElement<E> tempPreIndexElement = tempIndexElement.getLeft();
				
				
				for(E listElement : toAddList)
				{
					if(listElement != null)
					{
						RingBufferListElement<E> tempElement = new RingBufferListElement<E>(listElement);
						
						tempElement.setRight(tempIndexElement);
						tempElement.setLeft(tempIndexElement.getLeft());
						
						tempElement.getLeft().setRight(tempElement);
						tempElement.getRight().setLeft(tempElement);
						
						toRet = true;
					}
				}
				
				if(index == 0) this.currentPosition = tempPreIndexElement.getRight();
			}
		}
		
		return toRet;
	}
	
	public boolean removeAll(Collection<?> toSearchRemoveIn)
	{
		if(toSearchRemoveIn == null) return false;
		
		if(this.currentPosition != null)
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			RingBufferListElement<E> tempWork = tempBuf;
			
			do {
				tempWork = tempBuf;
				tempBuf = tempBuf.getRight();
				
				if(toSearchRemoveIn.contains(tempWork.getElementPointer()))
					this.removeElement(tempWork);
			} while(tempBuf != this.currentPosition);
		}
		
		return true;
	}
	
	public boolean retainAll(Collection<?> toSearchContainIn)
	{
		if(toSearchContainIn == null) return false;
		
		if(this.currentPosition != null)
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			RingBufferListElement<E> tempWork = tempBuf;
			
			do {
				tempWork = tempBuf;
				tempBuf = tempBuf.getRight();
				
				if(!toSearchContainIn.contains(tempWork.getElementPointer()))
					this.removeElement(tempWork);
			} while(tempBuf != this.currentPosition);
		}
		
		return true;
	}
	
	public E set(int index, E element)
	{	
		RingBufferListElement<E> tempElement = this.setElement(this.getElement(index), element);
		
		return ((tempElement != null) ? tempElement.getElementPointer() : null);
	}
	
	public int setPosition(E e)
	{
		if(e == null) return 0;
		
		int indexE = this.indexOf(e);
		if(indexE < 0) return 0;
		
		this.currentPosition = this.getElement(indexE);
		
		return indexE;
	}
	
	public E setPosition(int direction)
	{
		this.currentPosition = this.getElement(direction);
		
		return ((this.currentPosition != null) ? this.currentPosition.getElementPointer() : null);
	}
	
	private RingBufferListElement<E> setElement(RingBufferListElement<E> position, E element)
	{
		if(element == null) return null;
		if(position == null) return null;
		
		RingBufferListElement<E> tempElement = new RingBufferListElement<E>(element);
		
		tempElement.setLeft(position.getLeft());
		tempElement.setRight(position.getRight());
		
		tempElement.getLeft().setRight(tempElement);
		tempElement.getRight().setLeft(tempElement);
		
		position.setLeft(null);
		position.setRight(null);
		
		return position;
	}
	
	private RingBufferListElement<E> getElement(int index)
	{
		if(this.currentPosition != null)
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			int sIndex = this.shortIndex(index);
			int counter = 0;
			
			do {
				if(counter == sIndex)
					return tempBuf;
				
				if(sIndex >= 0)
				{
					counter++;
					tempBuf = tempBuf.getRight();
				} else {
					counter--;
					tempBuf = tempBuf.getLeft();
				}
			} while(tempBuf != this.currentPosition);
		}
		
		return null;
	}
	
	private RingBufferListElement<E> getElement(E e)
	{
		if((this.currentPosition != null) && (e != null))
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			
			do {
				if(tempBuf.getElementPointer().compareTo(e) == 0)
					return tempBuf;
				
				tempBuf = tempBuf.getRight();
			} while(tempBuf != this.currentPosition);
		}
		
		return null;
	}
	
	private RingBufferListElement<E> getElement(Object o)
	{
		if((this.currentPosition != null) && (o != null))
		{
			RingBufferListElement<E> tempBuf = this.currentPosition;
			
			do {
				if(tempBuf.getElementPointer().equals(o))
					return tempBuf;
				
				tempBuf = tempBuf.getRight();
			} while(tempBuf != this.currentPosition);
		}
		
		return null;
	}

	private boolean removeElement(RingBufferListElement<E> tempElement)
	{
		if((this.currentPosition != null) && (tempElement != null))
		{
			tempElement.getLeft().setRight(tempElement.getRight());
			tempElement.getRight().setLeft(tempElement.getLeft());
				
			if(tempElement == this.currentPosition)
			{
				if(tempElement.getRight() != tempElement)
					this.currentPosition = tempElement.getRight();
				else
					this.currentPosition = null;
			}
				
			tempElement.setLeft(null);
			tempElement.setRight(null);
				
			return true;
		}
	
		return false;
	}
	
	private int shortIndex(int index)
	{
		return ((index != 0) ? (index % this.getSize()) : 0);
	}
}